## Beispiel zur Ansteuerung des Servomotors

Dokumentation Erweiterungsplatine: https://joy-it.net/files/files/Produkte/RB-Moto3/RB-Moto3_Anleitung_2022-03-25.pdf

### Kurzanleitung
1. Motorshield mt dem Raspberry Pi verbinden, mit Strom versorgen
2. Servomotor mit dem braunen Kabel nach unten an Channel 12 des Motorshields anschließen
3. SPI und I2C auf dem Raspberry Pi aktivieren
4. ``` sudo pip3 install smbus ```
5. [Python library](https://joy-it.net/files/files/Produkte/RB-Moto3/RB-Moto3_ServoLibrary_2022-03-25.zip) auf dem Raspberry Pi herunterladen
6. Library Installieren: In den Ordner navigieren und ``` sudo python3 setup.py install ```
 ausführen
7. simpletest.py ausführen

